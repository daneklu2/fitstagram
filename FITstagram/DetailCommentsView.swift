//
//  DetailCommentsView.swift
//  FITstagram
//
//  Created by Lukáš Daněk on 29.10.2021.
//

import SwiftUI

struct DetailCommentsView: View {
    let postId : String
    @State var postComments: [Comment] = []
    var body: some View {
        ScrollView {
            LazyVStack {
                ForEach(postComments, id: \.id) { comment in
                    HStack {
                        Rectangle()
                            .fill(Color.white)
                            .padding()
                            .aspectRatio(1, contentMode: .fill)
                            .frame(width: 40, height: 40)
                            .overlay(
                                RemoteImage(url: comment.author.avatar)
                                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                                    .aspectRatio(contentMode: .fill)
                            )
                            .clipped()
                        VStack {
                            HStack(alignment: .top) {
                                Text(comment.author.username)
                                    .fontWeight(.semibold)
                                +
                                Text( " " + comment.text)
                                Spacer()
                            }
                            HStack {
                                Text(String(comment.likes))
                                    .foregroundColor(Color.gray)
                                Text(" likes")
                                    .foregroundColor(Color.gray)
                                Spacer()
                            }
                        }
                        Spacer()
                    }
                    .padding(.horizontal)
                }
            }
        }
        .onAppear {
            fetchComments()
        }
    }
    
    private func fetchComments() {
        let url = URL(string: "https://fitstagram.ackee.cz/api/feed/" + postId + "/comments")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else { return }

            postComments = try! JSONDecoder().decode([Comment].self, from: data)
        }
        task.resume()
    }
}

struct DetailCommentsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailCommentsView(postId: "1")
    }
}
