//
//  Comment.swift
//  FITstagram
//
//  Created by Igor Rosocha on 10/11/21.
//

import Foundation

struct Comment: Identifiable {
    let id: String
    let author: Author
    let likes: Int
    let text: String
}
extension Comment: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        author = try container.decode(Author.self, forKey: .author)
        let likes = try container.decode([Author].self, forKey: .likes)
        self.likes = likes.count
        text = try container.decode(String.self, forKey: .text)
    }
}
