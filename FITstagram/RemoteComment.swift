//
//  RemoteComment.swift
//  FITstagram
//
//  Created by Lukáš Daněk on 29.10.2021.
//

import SwiftUI

struct RemoteComment: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct RemoteComment_Previews: PreviewProvider {
    static var previews: some View {
        RemoteComment()
    }
}
