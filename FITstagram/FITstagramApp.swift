//
//  FITstagramApp.swift
//  FITstagram
//
//  Created by Lukáš Daněk on 28.10.2021.
//

import SwiftUI

@main
struct FITstagramApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
