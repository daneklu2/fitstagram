//
//  DetailView.swift
//  FITstagram
//
//  Created by Igor Rosocha on 10/6/21.
//

import SwiftUI

struct DetailView: View {
    var post: Post
    @State private var images: [Image]?
    var body: some View {
        VStack() {
            //avatar + name
            HStack (){
                Rectangle()
                    .fill(Color.white)
                    .padding()
                    .aspectRatio(1, contentMode: .fill)
                    .frame(width: 80, height: 80)
                    .overlay(
                        RemoteImage(url: post.author.avatar)
                            .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                            .aspectRatio(contentMode: .fit)
                    )
                    .clipped()
                Spacer()
                Text(post.author.username)
                    .fontWeight(.bold)
                Spacer()
                
            }
            .padding(.horizontal)
            //pageview fotek
            TabView {
                ForEach(0..<post.photos.count) { photo_index in
                    Rectangle()
                        .fill(Color.white)
                        .aspectRatio(1, contentMode: .fill)
                        .overlay(
                            RemoteImage(url: post.photos[photo_index])
                                .aspectRatio(contentMode: .fit)
                        )
                        .clipped()
                }
            }
            .tabViewStyle(PageTabViewStyle())
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            .aspectRatio(1, contentMode: .fill)

            //scrolling komentařů
            DetailCommentsView(postId: post.id)
        }
        .navigationTitle("Post Detail")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            DetailView(post: .dummy)
                .previewDevice("iPhone 12")
        }
    }
}
