//
//  PageView.swift
//  FITstagram
//
//  Created by Lukáš Daněk on 29.10.2021.
//
import SwiftUI


struct ImageView: View {
    let image: Image
    var body: some View {
        image
            .resizable()
            .aspectRatio(contentMode: .fit)
        
    }
}

struct PageView: View {
    @State private var images: [Image]?
    let urls: [URL]
    var body: some View {
        if let images = images {
            TabView {
                ForEach(0..<urls.count) { photo_index in
                    ImageView(image: images[photo_index])
                }
            }
            .tabViewStyle(PageTabViewStyle())
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            
        } else {
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
                .onAppear { fetchImages() }
        }
    }
    private func fetchImages() {
        //TODO: podle poctu fotek paralelně getni všechny fotky
        var fetchedImages: [Image]?
        DispatchQueue.global().async {
            for i in 0..<urls.count {
                let data = try! Data(contentsOf: urls[i])
                let uiImage = UIImage(data: data)!
                fetchedImages?.append(Image(uiImage: uiImage))
            }
            DispatchQueue.main.async {
                images = fetchedImages
            }
        }
    }
}

struct PageView_Previews: PreviewProvider {
    static var previews: some View {
        PageView(urls: [URL(string: "https://placeimg.com/320/320/nature")!, URL(string: "https://placeimg.com/160/320/nature")!, URL(string: "https://placeimg.com/320/160/nature")!, URL(string: "https://placeimg.com/320/640/nature")!])
    }
}
