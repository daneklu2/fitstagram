//
//  CommentsView.swift
//  FITstagram
//
//  Created by Igor Rosocha on 10/11/21.
//

import SwiftUI

let comments = [
    Comment(id: "1", author: Author.dummy, likes: 5, text: "Pěkný"),
    Comment(id: "2", author: Author.dummy, likes: 5, text: "Proč bych chodil do přírody, lalalalala..."),
    Comment(id: "3", author: Author.dummy, likes: 5, text: "🏔🌅💦")
]

struct CommentsView: View {
    @State var comments: [Comment]
    @State private var isNewCommentShown = false
    
    var body: some View {
        List(comments, id: \.id) { comment in
            Group {
                Text(comment.author.username)
                    .fontWeight(.semibold)
                + Text(" " + comment.text)
            }
        }
        .listStyle(.grouped)
//        .fullScreenCover(isPresented: $isNewCommentShown) {
//            NewCommentView(
//                onNewComment: { comments.append(Comment(username: "igor.rosocha", text: $0)) },
//                isPresented: $isNewCommentShown)
//        }
        .navigationBarTitle("Komentáře")
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: {
                    isNewCommentShown = true
                }) {
                    Image(systemName: "plus")
                }
            }
        }
    }
}

struct CommentsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            CommentsView(comments: comments)
        }
    }
}
